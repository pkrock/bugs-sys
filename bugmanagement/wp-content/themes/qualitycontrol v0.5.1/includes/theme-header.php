<?php

function qc_header(){

    ?>
    <div id="branding" role="banner">

        <a class="brand_bug">&nbsp;</a>
                                <?php qc_page_title(); ?>
        <?php $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div'; ?>
        <<?php echo $heading_tag; ?> id="site-title">
            <a href="<?php echo home_url( '/' ); ?>"><?php bloginfo( 'name' ); ?></a> <?php wp_title( '&rarr;', true, 'left' ); ?>
        </<?php echo $heading_tag; ?>>
        <div class="tagline"><?php bloginfo( 'description' ); ?></div>

    </div><!-- End #branding -->
    <?php

}
add_action('appthemes_header', 'qc_header');