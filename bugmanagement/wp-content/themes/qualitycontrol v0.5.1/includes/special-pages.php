<?php

function qc_get_ticket_page_id() {
	return QC_Pages::get_id( 'create-ticket.php' );
}

function qc_get_edit_profile_page_id() {
	return QC_Pages::get_id( 'edit-profile.php' );
}

function qc_get_edit_profile_url() {
	$page_id = qc_get_edit_profile_page_id();
	if ( $page_id )
		return get_permalink( $page_id );

	return get_edit_profile_url( get_current_user_id() );
}

class QC_Pages {
	private static $page_ids = array();

	function install() {
		$pages = array(
			'create-ticket.php' => __( 'Create Ticket', APP_TD ),
			'edit-profile.php' => __( 'Edit Profile', APP_TD ),
		);

		foreach ( $pages as $template => $title ) {
			if ( !self::get_id( $template ) ) {
				$page_id = wp_insert_post( array(
					'post_type' => 'page',
					'post_status' => 'publish',
					'post_title' => $title
				) );

				add_post_meta( $page_id, '_wp_page_template', $template );
			}
		}
	}

	function get_id( $template ) {
		if ( isset( self::$page_ids[$template] ) )
			return self::$page_ids[$template];

		$pages = get_posts( array(
			'post_type' => 'page',
			'meta_key' => '_wp_page_template',
			'meta_value' => $template,
			'posts_per_page' => 1
		) );

		if ( empty( $pages ) )
			$page_id = 0;
		else
			$page_id = $pages[0]->ID;

		self::$page_ids[$template] = $page_id;

		return $page_id;
	}
}

add_action( 'qc_activation', array( 'QC_Pages', 'install' ) );
