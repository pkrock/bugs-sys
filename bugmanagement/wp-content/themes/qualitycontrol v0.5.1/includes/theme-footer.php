<?php

/*
 * Default Footer for Quality Control
 */
function qc_footer(){

    ?>
     <ul>
        <li><?php _e( '<a href="http://www.appthemes.com/themes/qualitycontrol/">Issue Tracking Software</a> powered by <a href="http://wordpress.org">WordPress</a>', APP_TD ); ?></li>

        <li class="alignright"><?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds.</li>
    </ul>

    <?php
}
add_action('appthemes_footer', 'qc_footer');