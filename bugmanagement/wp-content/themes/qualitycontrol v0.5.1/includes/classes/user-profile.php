<?php

class QC_User_Profile {

	private static $error;

	function init() {
		add_action( 'init', array( __CLASS__, 'update_frontend' ) );
		add_action( 'qc_user_profile_notice', array( __CLASS__, 'show_notice' ) );
	}

	function update_frontend() {
		if ( !isset( $_POST['action'] ) || 'qc-edit-profile' != $_POST['action'] )
			return;

		check_admin_referer( 'qc-edit-profile' );

		require ABSPATH . '/wp-admin/includes/user.php';

		$r = edit_user( $_POST['user_id'] );

		if ( is_wp_error( $r ) ) {
			self::$error = $r->get_error_message();
		} else {
			wp_redirect( './?updated=true' );
			exit();
		}
	}

	function show_notice() {
		if ( !empty( self::$error ) ) {
?>
	<span class="error"><?php echo self::$error; ?></span>
<?php
		} elseif ( isset( $_GET['updated'] ) ) {
?>
	<span class="updated"><?php _e( 'User updated.', APP_TD ); ?></span>
<?php
		}
	}
}

QC_User_Profile::init();

