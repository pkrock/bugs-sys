<?php

class QC_Dashboard extends APP_Dashboard {

	function setup() {
		parent::setup();

		$this->args['page_title'] = __( 'Quality Control Dashboard', APP_TD );
		$this->args['menu_title'] = __( 'Quality Control', APP_TD );
	}

	function page_init() {
		wp_enqueue_style( 'qc-admin', get_template_directory_uri() . '/includes/admin/admin.css' );
		parent::page_init();
	}
}

new QC_Dashboard( false );

