<?php

/**
 * Limit who can create a ticket.
 * Defaults to people who can publish posts. Create a function
 * with this name inside a child theme to override.
 */
function qc_ticket_creation_cap() {
	return current_user_can( 'publish_posts' );
}

/**
 * Generates a message used when ticket attributes are changed.
 */
function _qc_get_message_diff( $added, $deleted ) {
		$msg = array();

		if ( !empty( $added ) ) {
			$list = '<em>' . implode( '</em>, <em>', $added ) . '</em>';
			$msg[] = sprintf( __( '%s added', APP_TD ), $list );
		}

		if ( !empty( $deleted ) ) {
			$list = '<em>' . implode( '</em>, <em>', $deleted ) . '</em>';
			$msg[] = sprintf( __( '%s removed', APP_TD ), $list );
		}

		return $msg;
}

