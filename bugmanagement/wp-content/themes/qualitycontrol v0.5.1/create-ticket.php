<?php
/**
 * Create a ticket.
 *
 * A custom page template that should be assigned to a page.
 *
 * Template Name: Create Ticket
 *
 * @package Quality_Control
 * @since Quality Control 0.1
 */
?>

<?php get_header( 'create-ticket' ); ?>

<div id="main" role="main">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<div id="ticket-manager" class="tabber">

			<?php get_template_part( 'templates/navigation', 'create-ticket' ); ?>

			<div class="panel">
<?php
	if ( qc_ticket_creation_cap() )
		get_template_part( 'templates/form', 'create-ticket' );
	else
		echo html( 'p class="no-cap"', __( 'You do not have permission to create tickets.', APP_TD ) );
?>
			</div>

		</div><!-- End #ticket-manager -->

	<?php endwhile; endif; ?>

</div><!-- End #main -->

<?php get_sidebar( 'create-ticket' ); ?>

<?php get_footer( 'create-ticket' ); ?>
