<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Quality_Control
 * @since Quality Control 0.1
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	// Add the blog name.
	bloginfo( 'name' );

	wp_title( '|', true, 'left' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', APP_TD ), max( $paged, $page ) );

	?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<!--[if lte IE 9]><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/styles/ie.css" type="text/css" media="screen" /><![endif]-->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_template_directory_uri(); ?>/styles/1140.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

        <?php appthemes_before(); ?>

        <?php appthemes_before_header(); ?>

        <div id="branding_container" class="container">
	    <div class="row">
                <div class="ninecol">
                    <?php appthemes_header(); ?>
				</div>

				<div id="current-user-box" class="threecol last">
					<?php echo get_avatar( is_user_logged_in() ? wp_get_current_user()->user_email : 0, 30 ); ?>

					<div id="current-user-name"><?php
						if ( is_user_logged_in() )
							echo wp_get_current_user()->display_name;
						else
							_e( 'Visitor', APP_TD );
					?></div>

					<div id="current-user-links"><?php
						if ( is_user_logged_in() ) {
							echo html_link( qc_get_edit_profile_url(), __( 'Edit profile', APP_TD ) );
							echo ' | ';
						}
						wp_loginout( get_bloginfo('url') );
					?></div>
                </div>
            </div>
        </div>

        <?php appthemes_after_header(); ?>

        <div id="content" class="container">
            <div class="row">
                <div class="ninecol">
