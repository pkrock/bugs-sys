<?php
/**
 * The template for displaying the footer.
 *
 * @package Quality_Control
 * @since Quality Control 0.1
 */
?>


        </div><!--.col-->
            
    </div><!--.row-->
    <div class="push"></div>
</div><!-- .container -->

<?php appthemes_before_footer(); ?>

<div id="footer" class="container">
    <div class="row">
    
        <div class="ninecol">
        
            <div id="footer-wrap">
                
               <?php appthemes_footer(); ?>
                
            </div>
            
            <?php wp_footer();?>
            
        </div><!--.col-->
	
        <div class="threecol last">
        
        </div><!--.col-->
            
    </div><!--.row-->
        
</div><!--#footer .container-->

<?php appthemes_after_footer(); ?>

<?php appthemes_after(); ?>
	
</body>

</html>
