<?php
/**
 * Edit user profile.
 *
 * A custom page template that should be assigned to a page.
 *
 * Template Name: Edit Profile
 *
 * @package Quality_Control
 * @since Quality Control 0.1
 */

appthemes_auth_redirect_login();

get_header( 'edit-profile' );
?>

<div id="main" role="main">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<div id="ticket-manager" class="tabber">

			<?php get_template_part( 'templates/navigation', 'edit-profile' ); ?>

			<div id="edit-profile" class="panel">
				<?php get_template_part( 'templates/form', 'edit-profile' ); ?>
			</div>

		</div><!-- End #ticket-manager -->

	<?php endwhile; endif; ?>

</div><!-- End #main -->

<?php get_sidebar( 'edit-profile' ); ?>

<?php get_footer( 'edit-profile' ); ?>
