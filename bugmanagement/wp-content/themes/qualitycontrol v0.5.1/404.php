<?php
/**
 * The Template for displaying the 404 Error Page
 *
 * @package Quality_Control
 * @since Quality Control 0.1
 */
?>

<?php get_header( '404' ); ?>

<h1 class="screen-reader-text"><?php _e( 'QC shared on W P L O C K E R . C O M - 404 - Page Not Found', APP_TD ); ?></h1>
<div id="message"><?php _e( 'Sorry, the page you are looking for could not be located.', APP_TD ); ?></div>

<?php get_sidebar( 'single' ); ?>

<?php get_footer( '404' ); ?>
