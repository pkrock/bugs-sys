<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Quality_Control
 * @since Quality Control 0.1
 */
?>

<?php get_header( 'search' ); ?>

<div id="main" role="main">

    <h2 class="screen-reader-text"><?php _e( 'Search Results', APP_TD ); ?></h2> 

    <div id="ticket-manager" class="tabber"> 
    
        <?php get_template_part( 'templates/navigation', 'search' ); ?>
        
        <div id="recent-tickets" class="panel"> 
        
            <ol class="ticket-list">
            
                <?php get_template_part( 'templates/loop', 'ticket' ); ?>
                
            </ol> 
            
        </div>
    
    </div><!-- End #ticket-manager --> 

</div><!-- End #main -->

<?php get_sidebar( 'search' ); ?>
		
<?php get_footer( 'search' ); ?>
